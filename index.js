const { App } = require('@slack/bolt');
const WebSocket = require('ws');


//
// Bridge methods
//

const noop = function () {};
const bridge = {
  sendToSlack: noop,
  sendToWeb: noop,
};


//
// Slack
//

const app = new App({
  token: process.env.BOT_TOKEN,
  appToken: process.env.SLACK_APP_TOKEN,
  socketMode: true,
});

(async () => {
  await app.start();
  // Receive
  app.message(async ({ message }) => {
    if (message.channel === process.env.SLACKBRIDGE_CHANNEL) {
      bridge.sendToWeb(message.text);
    }
  });
  // Send
  bridge.sendToSlack = async (message) => {
    try {
      const result = await app.client.chat.postMessage({
        channel: process.env.SLACKBRIDGE_CHANNEL,
        text: message,
      });
      console.log(result);
    }
    catch (error) {
      console.error(error);
    }
  };
})();


//
// Web
//

const wss = new WebSocket.Server({ port: process.env.PORT || 8080 });
const connections = new Set();

wss.on('connection', function handleConnection(ws) {
  connections.add(ws);
  // Receive
  ws.on('message', function handleMessage(message) {
    bridge.sendToSlack(message.toString());
  });
  ws.on('close', function close() {
    connections.delete(ws);
  });
});
// Send
bridge.sendToWeb = (message) => {
  for (const connection of connections) {
    connection.send(message);
  }
};
